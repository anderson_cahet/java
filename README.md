# Instructions:  
Download source from Bitbucket 
# You need:  
*Eclipse  
*Selenium  
*WebDriver  
*Chrome Driver  
*Maven  
- plugins should be in the pom.xml file already  
  
# On Terminal/CMD - Run mvn package to install packages 
  
Once in Eclipse:  
Open File: BrowserDriverBase:  
# String fileLocation —> update you "data.properties" location(check its properties with double/right mouse click) 
*data.properties: -> can select other browsers:  
# Note -> Safari does not close which cause problems while running multiple scenarios — Safari driver issue. -> can change other options as user email for test 
This framework can still be improved:  
*Login/-> can stay in only one class instead of in every class 
*Screenshots can be added to failed scenario  
*Reports can be sent once tests are completed Code can be improved, always.  
*Using Jenkins to program runs, which it was not so complicated to connect LOCALLY. 
# One suggestion is to developers to add 'data-reference', which usually is selected together with automation team -> that helps in saving time to locate unique ID’s. 
# Manual Scenarios were written in BDD using Gherkin syntax. 
- Manual scenarious sent by email 
# Tool used to save both manual/automated scenarios that I use is Testrails, there you can create your test cases, dividing by areas of the website with different repositories and where you create your proof of test, test run based in the code to be tested. It is also possible to create different types of reports. 
# Tool to report defects that I have knowledge are DevTrack and Jira 
  
Who do I talk to? 
Repo owner or admin 
# anderson.cahet@gmail.com 