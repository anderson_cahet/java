package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObject {
	
	public WebDriver driver;
	
	By _email_field = By.xpath("//*[@id='emailOrPhone']");
	By _password_field = By.xpath("//*[@id='password']");
	By _login_button_ = By.xpath("//div[span='Login']");
	By _select_modify_name = By.xpath("//a[@href='/profile-name-modify']");
	
	
	public LoginPageObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement getEmail() {
		return driver.findElement(_email_field);
	}
	
	public WebElement getPassword() {
		return driver.findElement(_password_field);
	}
	
	public WebElement selectLogin() {
		return driver.findElement(_login_button_);
	}
	
	public WebElement selectModifyName() {
		return driver.findElement(_select_modify_name);
		
	}
}
