package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProfileDetailsObject {
	//span.value
	public WebDriver driver;
	By _check_current_name_ = By.cssSelector("span.value");
	By _modify_first_name_ = By.cssSelector("#firstName");
	By _modify_last_name_ = By.cssSelector("#lastName");
	By _start_edit_name_ = By.cssSelector("span.value");
	By _done_edit_name_button_ = By.cssSelector(".btn.bRZVrW");
	
	public ProfileDetailsObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	public WebElement startEditButton() {
		return driver.findElement(_start_edit_name_);
	}
	public WebElement selectModifyFirstName() {
		return driver.findElement(_modify_first_name_);
		
	}
	
	public WebElement selectModifyLastName() {
		return driver.findElement(_modify_last_name_);
	}
	
	public WebElement doneEditName() {
		return driver.findElement(_done_edit_name_button_);
	}

	public WebElement getName() {
		return driver.findElement(_check_current_name_);
	}
}
