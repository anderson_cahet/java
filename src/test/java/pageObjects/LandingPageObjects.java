package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import resources.BrowserDriverBase;

public class LandingPageObjects extends BrowserDriverBase {
	
	public WebDriver driver;
	
	By _skip_button_ = By.cssSelector("button.btn-slider");
	By _next_button_ = By.cssSelector(".slick-arrow.slick-next");
	By _profile_button_ = By.cssSelector("ul.list-menu-dashboard:nth-child(1) > li:nth-child(1) > a:nth-child(1)");
	By _logout_button_ = By.cssSelector("ul.list-menu-dashboard:nth-child(1) > li:nth-child(6) > a:nth-child(1) > span:nth-child(1)");
	By _pop_up_button_ = By.cssSelector("button.btn-slider");
	By _profile_logout_button_ = By.cssSelector("div.logout");
	By _settings_button_ = By.cssSelector("nav > ul > li:nth-child(4) > a ");
	By _settings_edit_profile_ = By.cssSelector("li:nth-child(2) > a > div");
	
	public LandingPageObjects(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getSkip() {
		return driver.findElement(_skip_button_);
	}
	
	public WebElement getProfile() {
		return driver.findElement(_profile_button_);
	}
	
	public WebElement getSettings() {
		return driver.findElement(_settings_button_);
	}
	
	public WebElement getEditProfile() {
		return driver.findElement(_settings_edit_profile_);
	}
	
	public WebElement getLogout() {
		return driver.findElement(_logout_button_);
	}
	
	public WebElement getNextButton() {
		return driver.findElement(_next_button_);
	}
	
	public WebElement getPopUpNext() {
		return driver.findElement(_pop_up_button_);
	}
	
	public WebElement secondLogoutButton() {
		return driver.findElement(_profile_logout_button_);
	}
	
	

}
