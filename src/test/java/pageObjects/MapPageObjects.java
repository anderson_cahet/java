package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MapPageObjects {
	
	public WebDriver driver;
	
	By _map_selector = By.cssSelector("div.leaflet-container.leaflet-fade-anim.leaflet-grab.leaflet-touch-drag");
	
	public MapPageObjects(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement mapSelector() {
		return driver.findElement(_map_selector);
	}
}
