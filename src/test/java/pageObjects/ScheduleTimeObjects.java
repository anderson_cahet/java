package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ScheduleTimeObjects {
	
	public WebDriver driver;
	
	By _select_student_ = By.cssSelector("ul.list-options:nth-child(4) > li:nth-child(1) > a:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
	By _select_first_time_notification_ = By.cssSelector("div.wrap-main:nth-child(3) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)");
	By _select_second_time_notification_ = By.cssSelector("div.wrap-main:nth-child(4) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)");
	By _first_radio_button_ = By.cssSelector(".rc-dialog-body > ul:nth-child(2) > li:nth-child(1)");
	By _second_radio_button_ = By.cssSelector(".rc-dialog-body > ul:nth-child(2) > li:nth-child(2)");
	By _third_radio_button_ = By.cssSelector(".rc-dialog-body > ul:nth-child(2) > li:nth-child(3)");
	By _confirm_time_button_ = By.cssSelector(".button-wrapper > button:nth-child(2)");
	By _cancel_time_button_ = By.cssSelector(".button-wrapper > button:nth-child(1)");
	
	public ScheduleTimeObjects(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	public WebElement selectFirstStudent() {
		return driver.findElement(_select_student_);
	}
	
	public WebElement selectFirstScheduleTime() {
		return driver.findElement(_select_first_time_notification_);
	}
	
	public WebElement selectSecondScheduleTime() {
		return driver.findElement(_select_second_time_notification_);
	}
	
	public WebElement selectFirstRadioButton() {
		return driver.findElement(_first_radio_button_);
	}
	
	public WebElement selectSecondRadioButton() {
		return driver.findElement(_second_radio_button_);
	}
	
	public WebElement selectThirdRadioButton() {
		return driver.findElement(_third_radio_button_);
	}
	
	public WebElement selectDoneButton() {
		return driver.findElement(_confirm_time_button_);
	}
	
	public WebElement selectCancelButton() {
		return driver.findElement(_cancel_time_button_);
	}
}
