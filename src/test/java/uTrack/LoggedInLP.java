package uTrack;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import pageObjects.LandingPageObjects;
import pageObjects.LoginPageObject;
import resources.BrowserDriverBase;

public class LoggedInLP extends BrowserDriverBase {

	@BeforeSuite
	public void basePageNavigation() throws IOException {
		driver = initializerDriver();
		driver.get(prop.getProperty("url"));
	
	}
	
	@BeforeSuite
	public void loginFields() {
		LoginPageObject loginP = new LoginPageObject(driver);
		loginP.getEmail().sendKeys(prop.getProperty("emailLogin"));
		loginP.getPassword().sendKeys(prop.getProperty("emailPassword"));
		loginP.selectLogin().click();
		
		System.out.println("Login Done");
	}
	
	@BeforeSuite
	public void selectPopUp() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(5000);
		for (int i = 0; i < 2; i++) {
			landP.getNextButton().click();
			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		landP.getPopUpNext().click();
	}
	
	@Test
	public void selectProfile() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(2000);
		if (landP.getProfile().isDisplayed()) {
			landP.getProfile().click();
			System.out.println("select profile PRESENT");
		} else {
			System.out.println("select profile FAILED");
		}
	}
	
	@Test(dependsOnMethods = "selectProfile")
	public void returnToLoginPage() {
		LandingPageObjects landP = new LandingPageObjects(driver);
		landP.getLogout().click();
	
	}
	
	@AfterTest
    public void tearDown() {
      driver.quit();
	}
}
