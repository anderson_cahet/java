package uTrack;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import pageObjects.LandingPageObjects;
import pageObjects.LoginPageObject;
import pageObjects.ProfileDetailsObject;
import resources.BrowserDriverBase;

public class ModifyNameLP extends BrowserDriverBase {
	String originalName;
	String modifiedName;
	String firstName;
	String lastName;
	
	@BeforeSuite
	public void basePageNavigation() throws IOException {
		driver = initializerDriver();
		driver.get(prop.getProperty("url"));
		
	}
	@BeforeSuite
	public void loginFields() {
		LoginPageObject loginP = new LoginPageObject(driver);
		loginP.getEmail().sendKeys(prop.getProperty("emailLogin"));
		loginP.getPassword().sendKeys(prop.getProperty("emailPassword"));
		loginP.selectLogin().click();
		
		System.out.println("Email/Password/Login");
	}
	@Test
	public void selectPopUp() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(5000);
		for (int i = 0; i < 2; i++) {
			landP.getNextButton().click();
			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		landP.getPopUpNext().click();
	}
	
	@Test(dependsOnMethods = "selectPopUp")
	public void selectProfile() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(2000);
		if (landP.getProfile().isDisplayed()) {
			landP.getProfile().click();
			System.out.println("PRESENT");
		} else {
			System.out.println("FAILED");
		}
	}
	
	@Test(dependsOnMethods = "selectProfile")
	public void selectModifyName() throws InterruptedException {
		ProfileDetailsObject pd = new ProfileDetailsObject(driver);
		originalName = pd.startEditButton().getText();
		Thread.sleep(2000);
		if (pd.startEditButton().isDisplayed()) {
			pd.startEditButton().click();
			System.out.println("Edit PASS");
		} else {
			System.out.println("Edit FAIL");
		}
	}
	
	@Test(dependsOnMethods = "selectModifyName")
	public void modifyFirstName() throws InterruptedException {
		ProfileDetailsObject pd = new ProfileDetailsObject(driver);
		
		firstName = pd.selectModifyFirstName().getText();
		lastName = pd.selectModifyLastName().getText();
		pd.selectModifyFirstName().clear();
		pd.selectModifyFirstName().sendKeys(prop.getProperty("firstName"));
		pd.selectModifyLastName().clear();
		pd.selectModifyLastName().sendKeys(prop.getProperty("lastName"));
		pd.doneEditName().click();
		
		Thread.sleep(2000);
		modifiedName = pd.startEditButton().getText();
//		Fails if names are the same
//		Assert.assertEquals(modifiedName, originalName);
		Assert.assertNotSame(modifiedName, originalName);
		
	}
	
	@Test(dependsOnMethods = "modifyFirstName")
	public void checkModifiedName() throws InterruptedException {
		ProfileDetailsObject pd = new ProfileDetailsObject(driver);
		
		Thread.sleep(5000);
		pd.startEditButton().click();
		pd.selectModifyFirstName().clear();
		pd.selectModifyFirstName().sendKeys(prop.getProperty("regFirstName"));
		pd.selectModifyLastName().clear();
		pd.selectModifyLastName().sendKeys(prop.getProperty("regLastName"));
		pd.doneEditName().click();
	}
	
	@AfterTest
    public void tearDown() throws InterruptedException {
		Thread.sleep(2000);
		driver.quit();
	}
}