package uTrack;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import junit.framework.Assert;
import pageObjects.LandingPageObjects;
import pageObjects.LoginPageObject;
import pageObjects.MapPageObjects;
import resources.BrowserDriverBase;

public class MapLP extends BrowserDriverBase {
	
	@BeforeSuite
	public void basePageNavigation() throws IOException {
		driver = initializerDriver();
		driver.get(prop.getProperty("url"));
	}
	
	@BeforeSuite
	public void loginFields() {
		LoginPageObject loginP = new LoginPageObject(driver);
		loginP.getEmail().sendKeys(prop.getProperty("emailLogin"));
		loginP.getPassword().sendKeys(prop.getProperty("emailPassword"));
		loginP.selectLogin().click();
	}
	
	@Test
	public void test01Model() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(5000);
		for (int i = 0; i < 2; i++) {
			landP.getNextButton().click();
			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		landP.getPopUpNext().click();
	}
	
	@Test
	public void test02IsMapLoaded() {
		MapPageObjects mapO = new MapPageObjects(driver);
		if(mapO.mapSelector().isDisplayed() == true) {
			System.out.println("map has loaded");
		}
		
		Assert.assertEquals(true, true);
	}
	@AfterTest
    public void tearDown() throws InterruptedException {
		Thread.sleep(2000);
		driver.quit();
	}
	
	
}