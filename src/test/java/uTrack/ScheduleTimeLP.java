package uTrack;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import pageObjects.LandingPageObjects;
import pageObjects.LoginPageObject;
import pageObjects.ScheduleTimeObjects;
import resources.BrowserDriverBase;

public class ScheduleTimeLP extends BrowserDriverBase{
	
	@Test(priority = 1)
	public void basePageNavigation() throws IOException {
		driver = initializerDriver();
		driver.get(prop.getProperty("url"));
	}
	
	@Test(priority = 2)
	public void loginFields() {
		LoginPageObject loginP = new LoginPageObject(driver);
		loginP.getEmail().sendKeys(prop.getProperty("emailLogin"));
		loginP.getPassword().sendKeys(prop.getProperty("emailPassword"));
		loginP.selectLogin().click();
	}
	
	@Test(priority = 3)
	public void selectPopUp() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(5000);
		for (int i = 0; i < 2; i++) {
			landP.getNextButton().click();
			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		landP.getPopUpNext().click();
	}
	
	@Test(priority = 4)
	public void selectSettings() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(2000);
		if (landP.getSettings().isDisplayed()) {
			landP.getSettings().click();
			Thread.sleep(2000);
			landP.getEditProfile().click();
			System.out.println("select settings PRESENT");
		} else {
			System.out.println("select settings FAILED");
		}
	}
	
	@Test(priority = 5)
	public void selectStudent() {
		ScheduleTimeObjects st = new ScheduleTimeObjects(driver);
		st.selectFirstStudent().click();
	}
	@Test(priority = 6)
	public void selectTimeNotification() throws InterruptedException {
		ScheduleTimeObjects st = new ScheduleTimeObjects(driver);
		Thread.sleep(2000);
		st.selectFirstScheduleTime().click();
	}
	
	@Test(priority = 7)
	public void selectRadioButton() throws InterruptedException {
		ScheduleTimeObjects st = new ScheduleTimeObjects(driver);
		Thread.sleep(2000);
		
		if (!st.selectFirstRadioButton().isSelected()); {
			st.selectFirstRadioButton().click();
			Thread.sleep(2000);
		}
		if (!st.selectThirdRadioButton().isSelected()); {
			st.selectThirdRadioButton().click();
			Thread.sleep(2000);
		}
		if (!st.selectSecondRadioButton().isSelected()); {
			st.selectSecondRadioButton().click();
			Thread.sleep(2000);
			st.selectDoneButton().click();
		} 
	}
	
	@Test(priority = 8)
	public void confirmButtonSelected() throws InterruptedException {
		ScheduleTimeObjects st = new ScheduleTimeObjects(driver);
		Thread.sleep(2000);
		st.selectFirstScheduleTime().click();
		Assert.assertEquals(!st.selectSecondRadioButton().isSelected(), true, null);
		st.selectCancelButton().click();
	}
	
	@Test(priority = 9)
	public void tearDown() {
		driver.quit();
	}
}