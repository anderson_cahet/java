package uTrack;

import org.testng.annotations.Test;
import java.io.IOException;

import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.LandingPageObjects;
import pageObjects.LoginPageObject;
import resources.BrowserDriverBase;

public class FirstViewLP extends BrowserDriverBase {
	
	WebDriverWait wait;
	@Test
	public void Test01GetUrl() throws IOException {
		driver = initializerDriver();
		driver.get(prop.getProperty("url"));
		
	}
	
	@Test
	public void Test02Login() {
		LoginPageObject loginP = new LoginPageObject(driver);
		loginP.getEmail().sendKeys(prop.getProperty("emailLogin"));
		loginP.getPassword().sendKeys(prop.getProperty("emailPassword"));
		loginP.selectLogin().click();
		
		System.out.println("Email/Password/Login");
	}
	
	@Test
	public void Test03SkipModel() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(5000);
		landP.getSkip().click();
	}
	
	@Test
	public void Test04Logout() throws InterruptedException {
		LandingPageObjects landP = new LandingPageObjects(driver);
		Thread.sleep(5000);
		
		if (landP.getLogout().isDisplayed()) {
			landP.getLogout().click();
			System.out.println("Logged out PASS");
		} else {
			System.out.println("Logged out FAIL");
		}
		
	}
	
	@Test
    public void Test05Quit() throws InterruptedException {
		Thread.sleep(5000);
    		driver.quit();
    		System.out.println("tearDown method PASS");
	}
	
	
}